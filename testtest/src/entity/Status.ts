import{ Entity, PrimaryGeneratedColumn, Column, PrimaryColumn } from 'typeorm/browser'

//Entity('status')
export  class Status{
    
   @PrimaryGeneratedColumn('uuid')
    id: number

@Column("varchar")
    name: string
}