import{ Entity, PrimaryGeneratedColumn, Column, PrimaryColumn } from 'typeorm/browser'


//Entity('places')
export  class Places{
  
    @PrimaryGeneratedColumn("uuid")
    id: number

@Column("varchar")
    name: string

 @Column("varchar")
    shortName: string
}