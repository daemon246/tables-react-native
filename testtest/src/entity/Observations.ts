import{ Entity, PrimaryGeneratedColumn, Column, Timestamp, ManyToOne, OneToOne, JoinColumn, PrimaryColumn } from 'typeorm/browser'
import {Forms}  from './Forms'
import {Places}  from './Places'
import {Status} from './Status'


//Entity('observations')
export class Observations{
    
    @PrimaryGeneratedColumn("uuid") 
    id: number

    @Column({type: 'varchar'})
    date: string

    @Column({type: "varchar"})
    time: string
   
    @OneToOne(type => Places)
    @JoinColumn()
    PlaceID: Places[]

    @OneToOne(type => Forms)
    @JoinColumn()
    FormID: Forms[]

    @OneToOne(type => Status)
    @JoinColumn()
    StatusID: Status[]
}