import "reflect-metadata"
import{ Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, Table, EntitySchema } from 'typeorm'
import { type } from 'os';


@Entity('Forms')
export  class Forms{
   @PrimaryColumn({primary: true})
    id: number;

    @Column("varchar")
    book: string;
}
