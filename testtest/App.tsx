/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 
 * Generated with the TypeScript template
 * https://github.com/emin93/react-native-template-typescript
 * 
 * @format
 */

// [
//   "@babel/plugin-proposal-decorators",
//   {
//       "legacy": true
//   }
// ],
// [
//   "@babel/plugin-proposal-class-properties",
//   {
//       "loose": true
//   }
// ]


import "reflect-metadata"
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, FlatList } from 'react-native';
import { Forms } from './src/entity/Forms'
import { Observations } from './src/entity/Observations'
import { Places } from './src/entity/Places'
import { Status } from './src/entity/Status'
import { createConnection, getConnection, ConnectionManager, getRepository, EntitySchema, createQueryBuilder } from 'typeorm/browser/'






interface Props { }
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      table: {},
      id: -999

    }
  }
  //Подключение к БД
 dbconnect() {
   return createConnection({
     type: 'react-native',
    database: 'database',
     synchronize: true,
     location: 'default',
    logging: ['error', 'query', 'schema'],
     entities: [
       Observations,
       Places,
       Forms,
       Status
     ]
   });
 }

  //Показать таблицу целиком 
  /**
   * 
   * @param myTable - Таблица из БД
   */
  async read(myTable) {
    const table = await getConnection()
      .createQueryBuilder()
      .select()
      .from(myTable, '')
      .execute()


    this.setState({ table: table })
  }

  //Создать элемент в таблице
  /**
   * 
   * @param myTable - Таблица из БД
   * @param obj - Новый объект в таблице
   */
  async create(myTable, obj) {

    const table = await getConnection()
        .createQueryBuilder()
        .insert()
        .into(myTable)
        .values([
          obj
        ])
        .execute()

        this.setState({table: table})


  }
//Найти элемент таблицы по ID
/**
 * 
 * @param myTable - Таблица из БД
 * @param myID - ID строки, которую нужно спарсить
 */
  async readID(myTable, myID){
    
    const table = await getConnection()
     .createQueryBuilder()
     .select()
     .from(myTable, '')
     .where("id = :id", {id: myID})
     .execute()
     this.setState({table: table})
  }

//Удалить элемент в таблице по ID
/**
 * 
 * @param myTable - Таблица из БД
 * @param myID - ID строки, которую нужно удалить
 */
  async delete(myTable, myID)
  {
    
   await getConnection()
          .createQueryBuilder()
          .delete()
          .from(myTable) //table
          .where("id = :id", {id: myID}) //id
          .execute()
          
  }

  //Удалить данные из таблицы
  /**
   * 
   * @param myTable - Таблица из БД
   */
  async deleteAll(myTable){
    await getConnection()
               .createQueryBuilder()
               .delete()
               .from(myTable)
               .execute()
  }

  //Обновить данные в таблице по ID. (ID, новый объект)
/**
 * 
 * @param myTable - Таблица из БД
 * @param oldObj - ID данных, которые нужно изменить
 * @param newObj - новые данные
 */
  async update(myTable, oldObj, newObj){
    await getConnection()
                .createQueryBuilder()
                .update(myTable)
                .set(newObj)
                .where("id = :id", {id: oldObj})
                .execute()
                this.setState({table: table})
  }


  

//Подключение к БД при запуске
  componentDidMount() {
      this.dbconnect()
    
  }
  render = () =>{
    return (
      <View style={styles.container}>
        {console.log('--------------this.state.table--------------', this.state.table)}


        <Button onPress={this.read.bind(this, "Forms")} title="Open DB" />
        <Button onPress={this.create.bind(this, "Observations", {date: "28.09.20", time: "08.00", PlaceID: 1, FormID: 2, StatusID: 1})} title = 'Put in db' />
        <Button onPress={this.delete.bind(this, "Places", 8)} title = 'Delete string from db' />
        <Button onPress={this.deleteAll.bind(this, "Observations")} title="Delete all from table" />
        <Button onPress={this.readID.bind(this, "Places", 9)} title="Open string on ID" />
        <Button onPress={this.update.bind(this, "Places", 10, {shortName: "updname"} )} title="Update object"/>
        
      {
         this.state.table.length ? this.state.table.map( item => <Text key={item.id}>*{item.id}-{item.date}-{item.time} -{item.PlaceID}, {item.StatusID} - {item.FormID} </Text>) : null
      }
        
        
        

      </View>
    );
  }

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});